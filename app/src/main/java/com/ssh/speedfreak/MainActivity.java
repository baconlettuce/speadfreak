package com.ssh.speedfreak;

import android.support.v7.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements LabyrinthView.Callback {

    private LabyrinthView labyrinthView;

    private int stage;
    boolean isFinished = false;
    private int numBikeAccident;
    private int numCarAccident;

    private static final String EXTRA_KEY_STAGE = "key_stage";
    private static final String EXTRA_BIKE_ACCIDENT = "bike_accident";
    private static final String EXTRA_CAR_ACCIDENT = "car_accident";

    private static Intent newIntent(Context context, int stage, int numBikeAccident, int numCarAccident) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_KEY_STAGE, stage);
        intent.putExtra(EXTRA_BIKE_ACCIDENT, numBikeAccident);
        intent.putExtra(EXTRA_CAR_ACCIDENT, numCarAccident);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        stage = getIntent().getIntExtra(EXTRA_KEY_STAGE, 0);
        numBikeAccident = getIntent().getIntExtra(EXTRA_BIKE_ACCIDENT, 0);
        numCarAccident = getIntent().getIntExtra(EXTRA_CAR_ACCIDENT, 0);

        labyrinthView = new LabyrinthView(this);
        labyrinthView.setStage(stage);
        labyrinthView.setCallback(this);
        setContentView(labyrinthView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        labyrinthView.startSensor();
    }

    @Override
    protected void onPause() {
        super.onPause();

        labyrinthView.stopSensor();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGoal() {
        if (isFinished) {
            return;
        }
        isFinished = true;

        Toast.makeText(this, "ゴール!!", Toast.LENGTH_SHORT).show();

        labyrinthView.stopSensor();
        labyrinthView.stopDrawThread();

        nextStage();

        finish();
    }

    @Override
    public void onBikeAccident() {
        if (isFinished) {
            return;
        }
        isFinished = true;
        numBikeAccident++;

        String message = "自転車に衝突!! ("+numBikeAccident+"回目)";
        if (numBikeAccident % 7 == 0)
            message += "　下手クソ!";
        if (numBikeAccident % 11 == 0)
            message += "　免停になるぞ!";
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        labyrinthView.stopSensor();

        retryStage();

        finish();
    }

    @Override
    public void onCarAccident() {
        if (isFinished) {
            return;
        }
        isFinished = true;
        numCarAccident++;

        String message = "自動車に衝突!! ("+numCarAccident+"回目)";
        if (numCarAccident % 6 == 0)
            message += "　やる気あるかぁ？";
        if (numCarAccident % 13 == 0)
            message += "　ちっと手加減してやろうか？";
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        labyrinthView.stopSensor();

        retryStage();

        finish();
    }

    private void nextStage() {
        Intent intent = MainActivity.newIntent(this, stage + 1, numBikeAccident, numCarAccident);
        startActivity(intent);
    }

    private void retryStage() {
        Intent intent = MainActivity.newIntent(this, stage, numBikeAccident, numCarAccident);
        startActivity(intent);
    }
}
